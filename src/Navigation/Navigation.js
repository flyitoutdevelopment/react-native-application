import React, { useContext } from 'react';
// <<<<<<<<<<<<<<<<<<<<< NAVIGATION OF THE COMPLETE APP >>>>>>>>>>>>>>>>>>>>>>
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from '../screens/others/DrawerContent';
import { Image, TouchableOpacity } from 'react-native';
import HomeScreen from '../screens/others/HomeScreen';
import ProfileScreen from '../screens/others/ProfileScreen';
import ChatScreen from '../screens/others/ChatScreen';
import Add from '../screens/others/Add';
import MyEntryScreen from '../screens/others/MyEntryScreen';
import MyReceiverScreen from '../screens/others/MyReceiverScreen';
import SignInScreen from '../screens/authentication/SignInScreen';
import SignUpScreen from '../screens/authentication/SignUpScreen';
import Splash from '../screens/others/Splash';
import OtpScreen from '../screens/others/otpScreen';
import AddEntryScreen from '../screens/others/AddEntry';
import TravellMode from '../screens/others/TravellMode';
import AddFlightDetail from '../screens/others/AddFlightDetail';
import AddCarDetail from '../screens/others/AddCarDetails';
import AddBusDetail from '../screens/others/AddBusDetail';
import AddTrainDetail from '../screens/others/AddTrainDetail';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import CountryCode from '../screens/authentication/CountryCode';
import { Thumbnail } from 'native-base';
import EditBasicDetail from '../screens/others/EditBasicDetail';
import EditContactDetail from '../screens/others/EditContactDetail';
import EditPhoneNumber from '../screens/others/EditPhoneNumber';
import EditEmailDetails from '../screens/others/EditEmailDetail';
import EditBusinessDetails from '../screens/others/EditBusinessDetail';
import AddBusiness from '../screens/others/AddBusiness';
import AdressDetail from '../screens/others/AdressDetail';
import Preference from '../screens/others/Preferences';
import Address from '../screens/others/Address';
import SetLocation from '../screens/others/SetLocation';
import Invite from '../screens/others/Invite';
import AddTravel from '../screens/others/AddTravel';
import SenderAddEntry from '../screens/others/Senderaddentry';
import senderEntryBid from '../screens/others/senderEntryBid';
import UsersProfile from '../screens/others/UsersProfile'; //
import Review from '../screens/others/Review';
import MyReviews from '../screens/others/MyReviews';
import PreviewScreen from '../screens/others/EntryDetails';
import PublishEntry from '../screens/others/PublishEntry';
import Confirmbid from '../screens/others/ConfirmBid';
import AddItem from '../screens/others/AddItem';
import SelectAddress from '../screens/others/SelectAddress';
import SelectReciver from '../screens/others/SelectReceiver';
import TravelPreference from '../screens/others/TravelPreference';
import FlightTravel from '../screens/others/FlightTravel';
import TravelPreview from '../screens/others/TravelPreview';
import ChooseAddressList from '../screens/others/ChooseAddressList';
import ItemType from '../screens/others/ItemType';
import FTWebPage from '../screens/common/FTWebPage';
import InputStyles, { Text } from '../common/FTText';
import { color } from 'react-native-reanimated';
import { ThemeContext } from './../providers/theme-context';
import HyperVerge from '../screens/others/HyperVerge';


const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
// <<<<<<<<<<<<<<<< MAIN STACK NAVIGATOR>>>>>>>>>>>>>>>>>
function MyStack() {
  const themeContext = useContext(ThemeContext);
  const theme = themeContext.theme;

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
        headerBackTitleVisible: false,
        headerLeftContainerStyle: { marginLeft: 16 },
        headerRightContainerStyle: { marginRight: 16 },
        headerTitleStyle: InputStyles.semiBold14,
        headerBackImage: () => <MaterialIcons name="arrow-back" color={theme.black} size={24} />
      }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SignUpScreen"
          component={SignUpScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SignInScreen"
          component={SignInScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="HomeDrawer"
          component={HomeDrawer}
          options={{ headerShown: false }}
        />

        <Stack.Screen  
          name="HyperVerge"
          component={HyperVerge}
          options={{headerTitle: 'Upload your Document'}}/>

        <Stack.Screen
          name="ItemType"
          component={ItemType}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Add Item"
          component={AddItem}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ChooseAddressList"
          component={ChooseAddressList}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MyReceiverScreen"
          component={MyReceiverScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="UsersProfile"
          component={UsersProfile}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Review"
          component={Review}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MyReviews"
          component={MyReviews}
          options={{ headerShown: false }}
        />

        <Stack.Screen name="Confirm Bid" component={Confirmbid} />
        <Stack.Screen
          name="Publish Entry"
          component={PublishEntry}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Entry Details"
          component={PreviewScreen}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="TravelPreview"
          component={TravelPreview}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="FlightTravel"
          component={FlightTravel}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="TravelPreference"
          component={TravelPreference}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="AddTravel"
          component={AddTravel}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Invite"
          component={Invite}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Select Receiver"
          component={SelectReciver}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Select Address"
          component={SelectAddress}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="senderEntryBid"
          component={senderEntryBid}
          options={{ title: 'Entry Details' }}
        />

        <Stack.Screen
          name="SenderAddEntry"
          component={SenderAddEntry}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="Set Location"
          component={SetLocation}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Address"
          component={Address}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Preference" component={Preference} />
        <Stack.Screen
          name="Adress Detail"
          component={AdressDetail}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Add Business"
          component={AddBusiness}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Business Detail"
          component={EditBusinessDetails}
          options={{
            headerRight: () => {
              return (
                <TouchableOpacity
                  onPress={() => props.navigation.navigate('Add Business')}>
                  <Text style={[InputStyles.bold14], { color: theme.primary, }}>Add</Text>
                </TouchableOpacity>
              )
            }
          }}
        />
        <Stack.Screen
          name="Phone Number"
          component={EditPhoneNumber}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Email Detail" component={EditEmailDetails} />
        <Stack.Screen name="Contact Details" component={EditContactDetail} />
        <Stack.Screen name="Basic Details" component={EditBasicDetail} />
        <Stack.Screen
          name="CountryCode"
          mode="modal"
          component={CountryCode}
          options={{
            headerShown: true,
            title: "Choose Country",
          }}
        />
        <Stack.Screen name="Verification" component={OtpScreen} />

        <Stack.Screen
          name="AddEntryScreen"
          component={AddEntryScreen}
          options={{ headerTitle: 'Add Entry' }}
        />
        <Stack.Screen
          name="TravellMode"
          component={TravellMode}
          options={{ headerTitle: 'Travel Mode' }}
        />
        <Stack.Screen
          name="AddFlightDetail"
          component={AddFlightDetail}
          options={{ headerTitle: 'Add Flight Detail' }}
        />
        <Stack.Screen
          name="AddCarDetail"
          component={AddCarDetail}
          options={{ headerTitle: 'Add Car Detail' }}
        />
        <Stack.Screen
          name="AddBusDetail"
          component={AddBusDetail}
          options={{ headerTitle: 'Add Bus Detail' }}
        />
        <Stack.Screen
          name="AddTrainDetail"
          component={AddTrainDetail}
          options={{ headerTitle: 'Add Train Detail' }}
        />
        <Stack.Screen
          name="FTWebPage"
          component={FTWebPage}
          options={({ route }) => ({
            title: route.params.name
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

// <<<<<<<<<<<<<<<<MY TABS>>>>>>>>>>>>>>>>>
function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#8C57F0"
      inactiveColor="black">
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: null,
          tabBarColor: 'transparent',
          tabBarIcon: ({ color }) =>
            color === 'black' ? (
              <Image
                source={require('images/homeunselected.png')}
                style={{ height: 24, width: 24 }}
              />
            ) : (
              <Image
                source={require('images/icnhome.png')}
                style={{ height: 24, width: 24 }}
              />
            ),
        }}
      />
      <Tab.Screen
        name="ChatScreen"
        component={ChatScreen}
        options={{
          tabBarLabel: null,
          tabBarColor: 'transparent',
          tabBarIcon: ({ color }) =>
            color === 'black' ? (
              <Image
                source={require('images/Iconunselected.png')}
                style={{ height: 24, width: 24 }}
              />
            ) : (
              <Image
                source={require('images/messageselected.png')}
                style={{ height: 24, width: 24 }}
              />
            ),
        }}
      />
      <Tab.Screen
        name="Add"
        component={Add}
        options={{
          tabBarLabel: null,
          tabBarColor: 'transparent',
          tabBarIcon: ({ color }) => (
            <TouchableOpacity
              style={{
                flex: 1,
                width: 50,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'transparent',
                marginTop: -10,
              }}>
              <MaterialIcons name="add-circle" color="#8C57F0" size={35} />
            </TouchableOpacity>
          ),
        }}
      />

      <Tab.Screen
        name="MyEntryScreen"
        component={MyEntryScreen}
        options={{
          tabBarLabel: null,
          tabBarColor: 'transparent',
          tabBarIcon: ({ color }) =>
            color === 'black' ? (
              <Image
                source={require('images/entriesunselected.png')}
                style={{ height: 24, width: 24 }}
              />
            ) : (
              <Image
                source={require('images/Myentriesselected.png')}
                style={{ height: 24, width: 24 }}
              />
            ),
        }}
      />

      <Tab.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{
          tabBarLabel: null,
          tabBarColor: 'transparent',
          tabBarIcon: ({ color }) => (
            <Thumbnail
              source={require('images/Ellipse.png')}
              style={{ width: 30, height: 30 }}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

// <<<<<<<<<<<<<<<<DRAWER NAVIGATOR>>>>>>>>>>>>>>>>>
const HomeDrawer = () => {
  return (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="MyTabs" component={MyTabs} />
    </Drawer.Navigator>
  );
};

export default MyStack;