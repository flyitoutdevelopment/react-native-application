import React, { useContext } from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import { ThemeContext, colorWithAlpha, themes } from './../../src/providers/theme-context';
import TextStyles from 'textStyle';

export default function FTButton({ label, onPress }) {
  const themeContext = useContext(ThemeContext);
  const theme = themeContext.theme;

  return (
    <View
      style={{
        width: '100%',
        height: 44,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 22,
        backgroundColor: theme.primary, shadowColor: colorWithAlpha(theme.primary, 0.25), shadowOffset: { width: 0, height: 5 }, shadowOpacity: 0.15
      }}>
      <TouchableOpacity onPress={onPress}>
        <Text style={[TextStyles.medium14, { color: theme.white }]} >{label}</Text>
      </TouchableOpacity>
    </View>
  );
}