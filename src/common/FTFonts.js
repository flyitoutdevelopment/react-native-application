"use strict";

import { Platform, Dimensions } from "react-native";

const DEVICE_SCALE = Dimensions.get("window").width / 375;
const defaultFont = "Comfortaa";

/* utils ==================================================================== */

function normalize(size): Number {
  return Math.round(DEVICE_SCALE * size);
}

function lineHeight(
  val,
  normalized = true
): Number {
  let adjusted = normalized ? normalize(val) : val;
  return adjusted;
}

export {
  defaultFont,
  normalize,
  lineHeight
}