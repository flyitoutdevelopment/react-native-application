import React, { useContext } from "react";
import { View, TextInput, StyleSheet, TouchableOpacity } from "react-native";
import { Validations } from 'textInput';
import { ThemeContext, colorWithAlpha, themes } from './../../src/providers/theme-context';
import TextStyles, { Text } from 'textStyle';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Input = ({
    inputStyle,
    touched,
    error,
    ...props
}) => {
    const themeContext = useContext(ThemeContext);
    const theme = themeContext.theme;
    const borderColor = (error && touched) ? theme.paleRed : colorWithAlpha(theme.complement, 0.1);

    return (
        <View style={{ height: 72 }}>
            <View style={{ height: 44 }}>
                <TextInput style={[InputStyles.textInput, inputStyle, TextStyles.medium14, { borderColor: borderColor, borderWidth: 2, backgroundColor: colorWithAlpha(theme.complement, 0.1) }]} ref={props.reference} {...props} />
            </View>
            <Text style={[InputStyles.errorText, TextStyles.semiBold10, { color: theme.paleRed }]} numberOfLines={2}>{touched && error}</Text>
        </View>
    );
};

export const MobileInput = ({
    inputStyle,
    touched,
    error,
    ...props
}) => {
    const themeContext = useContext(ThemeContext);
    const theme = themeContext.theme;
    const borderColor = (error && touched) ? theme.paleRed : colorWithAlpha(theme.complement, 0.1);

    return (
        <View style={{ height: 72, flexDirection: 'column' }}>
            <View style={[InputStyles.mobileTextView, { borderColor: borderColor, borderWidth: 2, height: 44, backgroundColor: colorWithAlpha(theme.complement, 0.1) }]}>
                <TouchableOpacity
                    onPress={props.moveToCountryCode}
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                    <Text style={TextStyles.medium14} allowFontScaling={false}>{props.countryCode} </Text>
                    <MaterialIcons name="keyboard-arrow-down" size={24} color={theme.black} />
                    <View style={{ width: 6, }} />
                    <View style={{ width: 1, backgroundColor: theme.lightBlack, paddingVertical: 14 }} />
                </TouchableOpacity>
                <TextInput
                    style={[InputStyles.mobileTextInput, TextStyles.medium14, inputStyle, { height: 40 }]}
                    keyboardType="number-pad"
                    {...props}
                    ref={props.reference}
                    allowFontScaling={false}
                    multiline={false}
                    maxLength={props.maxLength}
                    // onEndEditing={props.onEndEditing(number)}
                    onSubmitEditing={props.onSubmitEditing}
                    onChangeText={props.onChangeText}
                />
            </View>
            <Text style={[InputStyles.errorText, TextStyles.semiBold10, { color: theme.paleRed }]} numberOfLines={2}>{touched && error}</Text>
        </View>
    );
};

export default Input;


export const InputStyles = StyleSheet.create({
    mobileTextView: {
        flexDirection: 'row',
        borderRadius: 22,
        alignItems: 'center',
        paddingHorizontal: 20
    },
    mobileTextInput: {
        flex: 1,
        backgroundColor: 'transparent',
        paddingLeft: 14
    },
    textInput: {
        flex: 1,
        borderRadius: 22,
        paddingHorizontal: 20,
        paddingVertical: 6
    },
    errorText: {
        paddingHorizontal: 20,
        paddingTop: 6
    },
});