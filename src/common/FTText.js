
"use strict";

// Dependencies
// =============================================================================

import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import * as FTFonts from "./FTFonts";
import ReactNative from "react-native";
import { ThemeContext } from './../providers/theme-context';

export const Text = ({ style, ...props }) => {
  const themeContext = useContext(ThemeContext);
  const theme = themeContext.theme;

  return (
    <ReactNative.Text style={[{ color: theme.black }, textStyles.text, style]} {...props} maxFontSizeMultiplier={1.6} allowFontScaling={props.allowFontScaling ?? true} />
  );
};

// Styles
// =============================================================================

const textStyles = StyleSheet.create({
  text: {
    fontFamily: FTFonts.defaultFont
  },
  bold24: {
    fontFamily: FTFonts.defaultFont,
    fontWeight: "bold",
    fontSize: FTFonts.normalize(24),
    lineHeight: FTFonts.lineHeight(24)
  },
  bold12: {
    fontFamily: FTFonts.defaultFont,
    fontWeight: "bold",
    fontSize: FTFonts.normalize(12),
    lineHeight: FTFonts.lineHeight(12)
  },
  bold14: {
    fontFamily: FTFonts.defaultFont,
    fontWeight: "bold",
    fontSize: FTFonts.normalize(14),
    lineHeight: FTFonts.lineHeight(14)
  },
  semiBold14: {
    fontFamily: FTFonts.defaultFont + "-SemiBold",
    fontSize: FTFonts.normalize(14),
    lineHeight: FTFonts.lineHeight(14)
  },
  semiBold10: {
    fontFamily: FTFonts.defaultFont + "-SemiBold",
    fontSize: FTFonts.normalize(10),
    lineHeight: FTFonts.lineHeight(10)
  },
  medium14: {
    fontFamily: FTFonts.defaultFont + "-Medium",
    fontSize: FTFonts.normalize(14),
    lineHeight: FTFonts.lineHeight(14)
  },
  regular10: {
    fontFamily: FTFonts.defaultFont + "-Regular",
    fontSize: FTFonts.normalize(10),
    lineHeight: FTFonts.lineHeight(14)
  },
  regular12: {
    fontFamily: FTFonts.defaultFont + "-Regular",
    fontSize: FTFonts.normalize(12),
    lineHeight: FTFonts.lineHeight(12)
  },
});

export default textStyles