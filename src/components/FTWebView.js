import { WebView } from 'react-native-webview';
import {
    View
} from 'react-native'

export default FTWebView = props => {
    return (
        <View style={{ flex: 1 }}>
            <WebView
                source={{
                    uri: props.route.params.sourceUri
                }}
                originWhitelist={['https://*']}
                style={{ flex: 1 }}
            />
        </View>
    )
}