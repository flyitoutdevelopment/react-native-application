import React, { useState } from "react";
import { useColorScheme } from 'react-native';
import { Appearance } from 'react-native';

export const themes = {
    light: {
        primary: "rgba(140, 87, 240, 1)",
        primaryLight: "rgba(178, 144, 255, 1)",
        primaryVeryLight: "rgba(206, 186, 250, 1)",
        complement: "rgba(198, 204, 221, 1)",
        white: "rgba(255, 255, 255, 1)",
        lightGrey: "rgba(234, 234, 234, 1)",
        grey: "rgba( 177, 177, 177, 1)",
        lightBlack: "rgba(108, 108, 108, 1)",
        black: "rgba(31, 27, 27, 1)",
        paleGrey: "rgba(247, 249, 253, 1)",
        paleRed: "rgba(221, 80, 64, 1)",
        leafyGreen: "rgba(78, 190, 57, 1)",
        macaroniAndCheese: "rgba(230, 163, 56, 1)"
    },
    dark: {
        primary: "rgba(140, 87, 240, 1)",
        primaryLight: "rgba(178, 144, 255, 1)",
        primaryVeryLight: "rgba(206, 186, 250, 1)",
        complement: "rgba(198, 204, 221, 1)",
        white: "rgba(255, 255, 255, 1)",
        lightGrey: "rgba(234, 234, 234, 1)",
        grey: "rgba( 177, 177, 177, 1)",
        lightBlack: "rgba(108, 108, 108, 1)",
        black: "rgba(31, 27, 27, 1)",
        paleGrey: "rgba(247, 249, 253, 1)",
        paleRed: "rgba(221, 80, 64, 1)",
        leafyGreen: "rgba(78, 190, 57, 1)",
        macaroniAndCheese: "rgba(230, 163, 56, 1)"
    },
};

export const ThemeContext = React.createContext({
    theme: themes.light,
    toggleTheme: () => { },
});

export const ThemeProvider = ({ children }) => {
    const scheme = useColorScheme();
    // FIXME: - Need to update terinary condition once support dark mode
    const [themeState, setTheme] = useState(scheme === 'dark' ? themes.light : themes.light);

    const toggleTheme = () => {
        // TODO: - For now we won't support dark theme, we can use it in future
        // const newtheme = (themeState === themes.dark) ? themes.light : themes.dark;
        // isDark = !isDark;
        // setTheme(newtheme);
    };

    // TODO: - For now we won't support dark theme, we can use it in future
    // Appearance.addChangeListener(({ colorScheme }) => {
    //     if (colorScheme === 'dark') {
    //         setTheme(themes.dark);
    //         isDark = true;
    //     } else {
    //         setTheme(themes.light);
    //         isDark = false;
    //     }
    // });

    return (
        <ThemeContext.Provider value={{ toggleTheme, theme: themeState }}>
            {children}
        </ThemeContext.Provider>
    );
};


export function colorWithAlpha(name = '', opacity = 1) {
    return name.split(", 1)").join(`, ${opacity})`);
}
