import React, { Component, useContext, useState } from 'react';
import { StyleSheet, View, TouchableOpacity, TextInput, FlatList, Text } from 'react-native';
import { useTranslation } from 'react-i18next';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ThemeContext, colorWithAlpha, themes } from './../../providers/theme-context';
import axios from 'axios';
import { useEffect } from 'react';

export default CountryCode = props => {
  const themeContext = useContext(ThemeContext);
  const theme = themeContext.theme;
  const { t, i18n } = useTranslation();
  const [text, setText] = useState('');
  const [filterArray, filterCountries] = useState({});
  const [selectedCountry, setSelectedCountry] = useState(props.route.params.code);

  const searchCountry = t('searchCountry');

  const [codeArr, setCountries] = useState([
    {
      code: '+7840',
      name: 'Abkhazia'
    },
    {
      code: '+93',
      name: 'Afghanistan'
    },
    {
      code: '+263',
      name: 'Zimbabwe'
    },
    {
      code: '+91',
      name: 'India'
    },
  ]);

  useEffect(() => {
    filterCountries(codeArr);
    console.log("useEffect");
  }, []);

  // await axios.get("/countrycodes")
  //   .then(async (response) => {
  //     setCountries(response);
  //   });

  selectItem = item => {
    setSelectedCountry(item);
    props.route.params.onSelect(item);
    props.navigation.goBack();
  };

  renderSeparator = () => {
    return (
      <View style={{ backgroundColor: theme.lightGrey, height: 1, paddingHorizontal: 20 }} />
    )
  }

  handleSearch = (text) => {
    if (text.trim() == "") {
      filterCountries(codeArr);
    } else {
      const formattedQuery = text.toLowerCase()
      const data = codeArr.filter((item) => {
        return item.name.toLowerCase().includes(formattedQuery)
      })
      filterCountries(data);
    }
  }

  return (
    <View style={{ paddingVertical: 16, backgroundColor: theme.white, flexDirection: 'column', flex: 1 }}>
      <View style={{
        paddingHorizontal: 20,
      }}>
        <View style={{
          flexDirection: 'row',
          paddingVertical: 10,
          backgroundColor: colorWithAlpha(theme.complement, 0.1),
          borderRadius: 6,
          alignItems: 'center',
          paddingHorizontal: 12,
          justifyContent: 'flex-start'
        }}>
          <MaterialIcons name="search" color={theme.black} size={24} />
          <TextInput
            value={text}
            autoCapitalize="none"
            flex={1}
            paddingHorizontal={12}
            placeholder={searchCountry}
            onChangeText={(value) => {
              handleSearch(value);
              setText(value);
            }}
          />
          <TouchableOpacity onPress={() => {
            handleSearch('');
            setText('');
          }}>
            <MaterialIcons name="close" color={theme.black} size={24} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ height: 16 }} />
      <View style={{ backgroundColor: theme.lightGrey, height: 1 }} />
      <View style={{ paddingVertical: 16, flex: 1, paddingHorizontal: 20 }}>
        <FlatList
          data={filterArray}
          keyExtractor={item => item.code}
          extraData={filterArray}
          ItemSeparatorComponent={renderSeparator}
          renderItem={({ item }) => {
            const isSelect = (item.code == selectedCountry.code);
            const icon = isSelect ? <MaterialIcons name="check-circle" color={theme.primary} size={24} /> : null;
            return (
              <TouchableOpacity onPress={() => selectItem(item)}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', flex: 1, height: 48 }}>
                  <Text style={{ color: isSelect ? theme.primary : theme.black }}>{item.name + " (" + item.code + ")"}</Text>
                  {icon}
                </View>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </View>
  );
}