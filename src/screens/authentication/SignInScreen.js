import React, { useEffect, useContext, useState } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  ImageBackground,
  Image,
  ScrollView,
  SafeAreaView,
  Dimensions,
  TextInput
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TextStyles, { Text } from 'textStyle';
import { useTranslation } from 'react-i18next';
import AppIcon from 'images/appicon.svg'
import { ThemeContext, colorWithAlpha, themes } from './../../providers/theme-context';
import * as Yup from 'yup';
import Input, { MobileInput, InputStyles } from '../../common/FTInput';
import FTButton from './../../common/FTButton';
import { useField, Form, FormikProps, Formik, useFormik } from 'formik';

const SignInScreen = props => {
  const [countryCode, setCountryCode] = useState({
    code: '+91',
    name: 'India',
  });
  const [number, setNumber] = React.useState();
  const { t, i18n } = useTranslation();
  const themeContext = useContext(ThemeContext);
  const theme = themeContext.theme;

  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
  const isMobile = t('enterValidMobile');
  const LoginSchema = Yup.object().shape({
    mobile: Yup.string().matches(phoneRegExp, isMobile).required(isMobile),
  });

  const userLogin = (mobile) => {
    // <<<<<<<<<<<<<<<USER LOGIN API>>>>>>>>>>>>>
    // await axios.post("login api", {
    //     number: countryCode.code+''+number,
    //   })
    //   .then(async (response) => {
    //     console.log(response)
    //   })
    //   .catch((error) => {
    //   console.log(error)
    //   });

    // FIXME: - Need to navigate after api sucsess
    props.navigation.navigate('Verification', {
      number: { code: countryCode.code, number: mobile ?? 123 },
    });
  }

  function onSelectCountry(item) {
    setCountryCode(item);
  };

  const moveToCountryCode = () => {
    props.navigation.navigate('CountryCode', { onSelect: onSelectCountry, code: countryCode });
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: theme.white, }}>
      <ScrollView style={{
        flexDirection: 'column',
        backgroundColor: theme.white,
        flex: 1
      }, { overflow: "hidden", }} contentContainerStyle={{ paddingVertical: 60, paddingHorizontal: 20 }}>
        <AppIcon width={50} height={50} />
        <View style={{ height: 32 }} />
        <Text style={TextStyles.bold24}>{t('welcomeBack')}</Text>
        <View style={{ height: 24 }} />
        <Text style={[TextStyles.bold12, { color: theme.lightBlack }]}>
          {t('loginWithPhone')}
        </Text>
        <View style={{ height: 24 }} />
        <Formik
          initialValues={{
            email: "",
            fullName: "",
            mobile: ""
          }}
          onSubmit={(values) => {
            userLogin(values.mobile);
          }}
          validationSchema={LoginSchema}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            touched,
            errors,
            isValid
          }) => {
            return (
              <View>
                <MobileInput
                  moveToCountryCode={moveToCountryCode}
                  navigation={props.navigation}
                  countryCode={countryCode.code}
                  onChangeText={handleChange("mobile")}
                  onBlur={handleBlur("mobile")}
                  value={values.mobile}
                  touched={touched.mobile}
                  error={errors.mobile}
                  reference={(input) => { this.mobileInputRef = input }}
                  autoCapitalize="none"
                  placeholder={t('phoneNumber')}
                  keyboardType="number-pad"
                  returnKeyType="done"
                  allowFontScaling={false}
                  multiline={false}
                />

                <View style={{ height: 15 }} />
                <FTButton label={'Get code'} onPress={isValid ? handleSubmit : () => { }}></FTButton>
              </View>
            );
          }}
        </Formik>
        <View style={{ height: 32 }} />
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: "baseline", flexWrap: 'wrap' }}>
          <Text style={TextStyles.regular12}>
            {t('dontHaveAccount')}{' '}
          </Text>
          <TouchableOpacity onPress={() => props.navigation.navigate("SignUpScreen")}>
            <Text style={[TextStyles.bold14, { color: theme.primary, }]}>{t('signup')}</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default SignInScreen;