import React, { useEffect, useState, useContext, useRef } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  Image,
  SafeAreaView,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AppIcon from 'images/appicon.svg'
import TextStyles, { Text } from 'textStyle';
import { useHeaderHeight } from '@react-navigation/stack';
import { useTranslation } from 'react-i18next';

import { ThemeContext, colorWithAlpha, themes } from './../../providers/theme-context';
import * as Yup from 'yup';
import Input, { MobileInput, InputStyles } from '../../common/FTInput';
import FTButton from './../../common/FTButton';
import { useField, Form, FormikProps, Formik, useFormik } from 'formik';

const SignUp = (props) => {
  const [countryCode, setCountryCode] = useState({
    code: '+91',
    name: 'India',
  });
  const { t, i18n } = useTranslation();
  const themeContext = useContext(ThemeContext);
  const theme = themeContext.theme;

  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
  const isNotEmpty = t('enterValidName');
  const isEmail = t('enterValidEmail');
  const isMobile = t('enterValidMobile');
  const SignupSchema = Yup.object().shape({
    fullName: Yup.string().min(2, isNotEmpty).required(isNotEmpty),
    email: Yup.string().email(isEmail).required(isEmail),
    mobile: Yup.string().matches(phoneRegExp, isMobile).required(isMobile),
  });

  const userSignup = (mobile) => {
    // await axios.post("register", {
    //     number: countryCode.code+''+number,
    //   })
    //   .then(async (response) => {
    //     console.log(response)
    //   })
    //   .catch((error) => {
    //   console.log(error)
    //   });

    // FIXME: - Need to navigate after api sucsess
    props.navigation.navigate('Verification', {
      number: { code: countryCode.code, number: mobile ?? 123 },
    });
  }

  function onSelectCountry(item) {
    setCountryCode(item);
  };

  const moveToCountryCode = () => {
    props.navigation.navigate('CountryCode', { onSelect: onSelectCountry, code: countryCode });
  }

  const navigateFTWebPage = (properties) => {
    // FIXME: - Need to navigate after api sucsess
    props.navigation.navigate("FTWebPage", {
      sourceUri: properties.sourceUri,
      name: properties.title
    });
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: theme.white, }}>
      <KeyboardAvoidingView
        behavior={Platform.select({ ios: 'padding' })}
        style={{ flex: 1, }}
        keyboardVerticalOffset={useHeaderHeight() + 16}>
        <ScrollView style={{ overflow: "hidden", }} contentContainerStyle={{ paddingVertical: 60, paddingHorizontal: 20 }}>
          <AppIcon width={50} height={50} />
          <View style={{ height: 32 }} />
          <Text style={TextStyles.bold24}>{t('signup')}</Text>
          <View style={{ height: 24 }} />
          <Text style={[TextStyles.bold12, { color: theme.lightBlack }]}>
            {t('enterBelowInfo')}
          </Text>
          <View style={{ height: 24 }} />
          <Formik
            initialValues={{
              email: "",
              fullName: "",
              mobile: ""
            }}
            onSubmit={(values) => {
              userSignup(values.mobile);
            }}
            validationSchema={SignupSchema}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              touched,
              errors,
              isValid
            }) => {
              return (
                <View>
                  <Input
                    onChangeText={handleChange("fullName")}
                    onBlur={handleBlur("fullName")}
                    value={values.fullName}
                    touched={touched.fullName}
                    error={errors.fullName}
                    autoCapitalize="words"
                    placeholder={t('fullName')}
                    returnKeyType="next"
                    // FIXME: - Need to validate with back end
                    maxLength={20}
                    onSubmitEditing={(val) => {
                      this.emailInputRef.focus();
                    }}
                    blurOnSubmit={false}
                  />
                  <View style={{ height: 15 }} />
                  <Input
                    onChangeText={handleChange("email")}
                    onBlur={handleBlur("email")}
                    value={values.email}
                    touched={touched.email}
                    error={errors.email}
                    reference={(input) => { this.emailInputRef = input }}
                    autoCapitalize="none"
                    placeholder={t('emailId')}
                    keyboardType="email-address"
                    returnKeyType="next"
                    onSubmitEditing={(val) => {
                      this.mobileInputRef.focus();
                    }}
                    blurOnSubmit={false}
                  />
                  <View style={{ height: 15 }} />
                  <MobileInput
                    moveToCountryCode={moveToCountryCode}
                    countryCode={countryCode.code}
                    onChangeText={handleChange("mobile")}
                    onBlur={handleBlur("mobile")}
                    value={values.mobile}
                    touched={touched.mobile}
                    error={errors.mobile}
                    reference={(input) => { this.mobileInputRef = input }}
                    autoCapitalize="none"
                    placeholder={t('phoneNumber')}
                    keyboardType="number-pad"
                    returnKeyType="done"
                    allowFontScaling={false}
                    multiline={false}
                  />
                  <View style={{ height: 15 }} />
                  <View style={{ flexDirection: 'row', alignItems: "baseline", flexWrap: 'wrap' }}>
                    <Text style={TextStyles.regular10}>
                      {t('byContinuingYouAgree')}{' '}
                    </Text>
                    <TouchableOpacity onPress={() => navigateFTWebPage({ sourceUri: "https://www.google.com", title: "Terms & Conditions" })}>
                      <Text
                        style={[TextStyles.bold12, { color: theme.primary }]}>
                        {t('termsAndConditions')}{' '}
                      </Text>
                    </TouchableOpacity>
                    <Text style={TextStyles.regular10}>
                      {t('andThe')}{' '}
                    </Text>
                    <TouchableOpacity onPress={() => navigateFTWebPage({ sourceUri: "https://www.google.com", title: "Privacy Policy" })}>
                      <Text
                        style={[TextStyles.bold12, { color: theme.primary }]} >
                        {t('privacyPolicy')}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ height: 24 }} />
                  <FTButton label={'Get code'} onPress={isValid ? handleSubmit : () => { }}></FTButton>
                </View>
              );
            }}
          </Formik>
          <View style={{ height: 24 }} />
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: "baseline", flexWrap: 'wrap' }}>
            <Text style={TextStyles.regular12} >
              {t('alreadyHaveAccount')}{' '}
            </Text>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("SignInScreen")}>
              <Text style={[TextStyles.bold14, { color: theme.primary, }]}>
                {t('login')}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default SignUp;