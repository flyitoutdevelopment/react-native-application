import React, { useContext, useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  Image,
  ScrollView,
} from 'react-native';
import { useTheme } from '@react-navigation/native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import VerificationImage from 'images/pic_verification.svg';
import EditImage from 'images/edit_solid.svg';
import { useTranslation } from 'react-i18next';
import { ThemeContext, colorWithAlpha, themes } from './../../providers/theme-context';
import TextStyles, { Text } from 'textStyle';
import FTButton from './../../common/FTButton';
import { Button } from 'native-base';

const RESEND_OTP_TIME_LIMIT = 30;

let resendOtpTimerInterval;

const OtpScreen = props => {
  const { colors } = useTheme();
  let data = props.route.params.number;
  const { t, i18n } = useTranslation();
  const themeContext = useContext(ThemeContext);
  const theme = themeContext.theme;
  const [code, setCode] = useState('')
  const [resendButtonDisabledTime, setResendButtonDisabledTime] = useState(
    RESEND_OTP_TIME_LIMIT,
  );

  useEffect(() => {
    startResendOtpTimer();

    return () => {
      if (resendOtpTimerInterval) {
        clearInterval(resendOtpTimerInterval);
      }
    };
  }, [resendButtonDisabledTime]);

  const startResendOtpTimer = () => {
    if (resendOtpTimerInterval) {
      clearInterval(resendOtpTimerInterval);
    }
    resendOtpTimerInterval = setInterval(() => {
      if (resendButtonDisabledTime <= 0) {
        clearInterval(resendOtpTimerInterval);
      } else {
        setResendButtonDisabledTime(resendButtonDisabledTime - 1);
      }
    }, 1000);
  };

  const Resend = () => {
    //<<<<<<<<<<<<<<<<<<RESEND API>>>>>>>>>>>>> 
    //  await axios.post("resend api", {
    //   number: data.code+''+data.number,
    // })
    // .then(async (response) => {
    //   console.log(response)
    // })
    // .catch((error) => {
    // console.log(error)
    // });
  }

  const Verify = () => {
    // <<<<<<<<<<<<<<<<<<VERIFY API>>>>>>>>>>>>>>>>>>>
    //  await axios.post("verify api here", {
    //   number: data.code+''+data.number,
    // })
    // .then(async (response) => {
    //   console.log(response)
    // })
    // .catch((error) => {
    // console.log(error)
    // });
    props.navigation.navigate("HomeDrawer")
  }
  return (
    <View style={{ flex: 1, backgroundColor: theme.white }}>
      <StatusBar barStyle="light-content" />
      <ScrollView>
        <View style={{
          flex: 1,
          paddingHorizontal: 16,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <View style={{ height: Dimensions.get('window').height * 0.1 }} />
          <VerificationImage />
          <View style={{ height: 24 }} />
          <Text
            style={TextStyles.medium14}>
            We just send a verification code your phone.{' '}
          </Text>
          <View style={{ height: 6 }} />
          <View style={{ flexDirection: 'row', alignContent: 'center', alignItems: 'center' }}>
            <Text
              style={{ color: '#8C57F0', fontWeight: 'bold', paddingVertical: 5 }}>
              {data.code} {data.number}
            </Text>
            <View style={{ width: 8 }} />
            <Button style={{ backgroundColor: 'transparent' }} onPress={() => props.navigation.goBack()} small={true}><EditImage /></Button>
          </View>
          <View style={{ height: 32 }} />
          <OTPInputView
            style={{ width: '80%', alignSelf: 'center', height: 52 }}
            pinCount={4}
            code={code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            onCodeChanged={code => { setCode(code) }}
            autoFocusOnLoad
            codeInputFieldStyle={TextStyles.medium14, {
              backgroundColor: colorWithAlpha(theme.complement, 0.1),
              color: theme.black,
              borderRadius: 6,
            }}
            codeInputHighlightStyle={TextStyles.medium14, {
              backgroundColor: colorWithAlpha(theme.complement, 0.1),
              color: theme.black,
              borderWidth: 2,
              borderRadius: 6,
              borderColor: theme.primary
            }}
            onCodeFilled={code => {
              console.log(`Code is ${code}, you are good to go!`);
            }}
          />
          <View style={{ height: 32 }} />
          <TouchableOpacity onPress={() => Resend()}>
            <Text style={{ fontSize: 14 }}> <Text style={[TextStyles.semiBold14, { color: colorWithAlpha(theme.primary, (resendButtonDisabledTime > 0) ? 0.5 : 1) }]}>Resend code </Text> {(resendButtonDisabledTime < 1) ? "" : 'in '.concat(resendButtonDisabledTime, (resendButtonDisabledTime > 1) ? " secs" : " sec")}</Text>
          </TouchableOpacity>
          <View style={{ height: 24 }} />
          <TouchableOpacity style={{ width: '100%', height: 50, justifyContent: 'center', alignItems: 'center', borderRadius: 25, backgroundColor: (code.length == 4) ? theme.primary : colorWithAlpha(theme.primary, 0.5) }} onPress={() => (code.length == 4) ? Verify() : {}}>
            <Text style={[TextStyles.medium14, { color: theme.white }]}>Verify code</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default OtpScreen;